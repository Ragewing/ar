﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
  public int index = 0;
  int x = 0;
  int y = 0;

  private System.Action<int, int> swapFunc = null;

  private float originX = 57.5f;
  private float originY = -69.5f;
  private float offset = 48.5f;

  public void Init(int index, int i, int j, System.Action<int, int> swapFunc)
  {
    this.index = index;

    Mesh mesh = Resources.Load<Mesh>("" + index);
    this.GetComponent<MeshFilter>().sharedMesh = mesh;

    if (index == 16)
    {
      this.GetComponent<MeshFilter>().sharedMesh = null;
    }

    UpdatePos(i, j);

    this.swapFunc = swapFunc;

    this.gameObject.AddComponent<BoxCollider>();
  }

  public void UpdatePos(int i, int j)
  {
    this.x = i;
    this.y = j;
    this.gameObject.transform.localPosition = new Vector3(originX - j * offset, 0, originY + i * offset);
  }

  private void OnMouseDown()
  {
    if (Input.GetMouseButtonDown(0) && swapFunc != null)
    {
      swapFunc(x, y);
    }
  }

  public bool IsEmpty()
  {
    return index == 16;
  }
}
