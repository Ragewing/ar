﻿using System;
using System.Linq;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
  public Piece piecePrefab;

  public Piece[,] pieces = new Piece[4,4];
  private Piece[] flattenArray = new Piece[0];

  private GameObject winText;

  void Start()
  {
    Init();
  }

  void Init()
  {
    GameObject puzzleBox = GameObject.Find("puzzleBox");

    winText = GameObject.Find("winText");
    winText.SetActive(false);

    int idx = 0;
    for (int x = 0; x < 4; ++x)
    {
      for (int y = 0; y < 4; ++y)
      {
        Piece piece = Instantiate(piecePrefab, new Vector2(x, y), Quaternion.identity);
        piece.transform.parent = puzzleBox.transform;
        piece.Init(++idx, x, y, ClickToSwap);
        pieces[x, y] = piece;
      }
    }

    flattenArray = to1DArray(pieces);
  }

  void ClickToSwap(int x, int y)
  {
    int dx = getDx(x, y);
    int dy = getDy(x, y);

    var src = pieces[x, y];
    var dst = pieces[x + dx, y + dy];

    pieces[x, y] = dst;
    pieces[x + dx, y + dy] = src;

    src.UpdatePos(x + dx, y + dy);
    dst.UpdatePos(x, y);

    flattenArray = to1DArray(pieces);

    if (isVictoryAchieved())
    {
      winText.SetActive(true);
    }
    else
    {
      winText.SetActive(false);
    }
  }

  bool isVictoryAchieved()
  {
    for (int i = 0; i < flattenArray.Length; ++i)
    {
      if (i < flattenArray.Length - 1)
      {
        if (flattenArray[i].index - flattenArray[i + 1].index != -1)
          return false;
      }
    }

    return true;
  }

  int getDx(int x, int y)
  {
    // right empty
    if (x < 3 && pieces[x + 1, y].IsEmpty())
      return 1;

    // left empty
    if (x > 0 && pieces[x - 1, y].IsEmpty())
      return -1;

    return 0;
  }

  int getDy(int x, int y)
  {
    // top empty
    if (y < 3 && pieces[x, y + 1].IsEmpty())
      return 1;

    // bottom empty
    if (y > 0 && pieces[x, y - 1].IsEmpty())
      return -1;

    return 0;
  }

  private Piece[] to1DArray(Piece[,] input)
  {
    int size = input.Length;
    Piece[] result = new Piece[size];

    int write = 0;
    for (int i = 0; i <= input.GetUpperBound(0); ++i)
    {
      for (int z = 0; z <= input.GetUpperBound(1); ++z)
      {
        result[write++] = input[i, z];
      }
    }

    return result;
  }

  private Piece[,] to2DArray(Piece[] input, int height, int width)
  {
    Piece[,] output = new Piece[height, width];
    for (int i = 0; i < height; ++i)
    {
      for (int j = 0; j < width; ++j)
      {
        output[i, j] = input[i * width + j];
      }
    }
    return output;
  }

  public void Shuffle()
  {
    System.Random r = new System.Random();

    int[] numbers = new int[16];

    for (int i = 0; i < numbers.Length; ++i)
    {
      var next = 0;
      while (true)
      {
        next = r.Next(1, 17);
        if (!Contains(numbers, next)) break;
      }

      numbers[i] = next;
    }

    for (int x = 0; x < 4; ++x)
    {
      for (int y = 0; y < 4; ++y)
      {
        pieces[x, y].Init(numbers[x * 4 + y], x, y, ClickToSwap);
      }
    }

    winText.SetActive(false);
  }

  static bool Contains(int[] array, int value)
  {
    for (int i = 0; i < array.Length; i++)
    {
      if (array[i] == value) return true;
    }
    return false;
  }
}
